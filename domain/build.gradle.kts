//                                  Bismillahirrahmanirrahim


import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    kotlin("kapt")
    kotlin("plugin.serialization")
}

val rest_api_version: String by project
group = "com.flife"
version = rest_api_version

tasks.withType<KotlinCompile>().all {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

val dagger_version: String by project
val coroutines_version: String by project
val serialization_version: String by project
val h2_version: String by project
dependencies {
    //Kotlinx Coroutines
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-jvm:$coroutines_version")

    //Kotlinx Serialization
    implementation("org.jetbrains.kotlinx:kotlinx-serialization-core-jvm:$serialization_version")

    //H2 Database
    implementation("com.h2database:h2:$h2_version")

    //jBCrypt
    implementation("de.svenkubiak:jBCrypt:0.4.3")

    api(project(":data"))

    //Dagger
    kapt("com.google.dagger:dagger-compiler:$dagger_version")

    testImplementation("junit", "junit", "4.13")
}