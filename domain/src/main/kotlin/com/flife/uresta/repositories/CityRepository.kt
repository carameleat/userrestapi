/*
 * Copyright 2021 F: Life
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                      Bismillahirrahmanirrahim

package com.flife.uresta.repositories

import com.flife.uresta.daos.CityDao
import com.flife.uresta.models.*
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
interface CityRepository : CommonRepository<RawCity, City> {

    override suspend fun create(city: RawCity): Int

    override suspend fun update(city: RawCity): Boolean

    override suspend fun deleteBy(id: Int): Boolean

    override fun getAll(): Flow<City>

    override fun getBy(id: Int): Flow<City?>
}

class CityRepositoryImpl @Inject constructor(
    private val dao: CityDao
) : CityRepository {

    override suspend fun create(city: RawCity): Int {
        val base = city.toBaseRawCity()

        return dao.create(base)
    }

    override suspend fun update(city: RawCity): Boolean {
        val base = city.toBaseRawCity()

        return dao.update(base)
    }

    override suspend fun deleteBy(id: Int): Boolean {
        return dao.deleteBy(id)
    }

    override fun getAll(): Flow<City> {
        return dao.getAll().map { base -> base.toCity() }
    }

    override fun getBy(id: Int): Flow<City?> {
        return dao.getBy(id).map { base -> base?.toCity() }
    }
}

private fun RawCity.toBaseRawCity() = BaseRawCity(id, name, description, countryId)

private fun BaseCity.toCity() = City(
    id = id,
    name = name,
    description = description,
    country = country.toBriefCountry()
)

internal fun BaseBriefCity.toBriefCity() = BriefCity(id, name)