/*
 * Copyright 2021 F: Life
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                              Bismillahirrahmanirrahim

package com.flife.uresta.repositories

import com.flife.uresta.daos.CountryDao
import com.flife.uresta.models.BaseBriefCountry
import com.flife.uresta.models.BaseCountry
import com.flife.uresta.models.BriefCountry
import com.flife.uresta.models.Country
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
interface CountryRepository : CommonRepository<Country, Country> {

    override suspend fun create(country: Country): Int

    override suspend fun update(country: Country): Boolean

    override suspend fun deleteBy(id: Int): Boolean

    override fun getAll(): Flow<Country>

    override fun getBy(id: Int): Flow<Country?>
}

class CountryRepositoryImpl @Inject constructor(
    private val dao: CountryDao
) : CountryRepository {

    override suspend fun create(country: Country): Int {
        val base = country.toBaseCountry()

        return dao.create(base)
    }

    override suspend fun update(country: Country): Boolean {
        val base = country.toBaseCountry()

        return dao.update(base)
    }

    override suspend fun deleteBy(id: Int): Boolean {
        return dao.deleteBy(id)
    }

    override fun getAll(): Flow<Country> {
        return dao.getAll().map { base -> base.toCountry() }
    }

    override fun getBy(id: Int): Flow<Country?> {
        return dao.getBy(id).map { base -> base?.toCountry() }
    }
}

private fun Country.toBaseCountry() = BaseCountry(id, name, description)

private fun BaseCountry.toCountry() = Country(id, name, description)

internal fun BaseBriefCountry.toBriefCountry() = BriefCountry(id, name)