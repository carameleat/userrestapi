/*
 * Copyright 2021 F: Life
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim

package com.flife.uresta.repositories

import com.flife.uresta.daos.UserDao
import com.flife.uresta.enums.Gender
import com.flife.uresta.models.BaseRawUser
import com.flife.uresta.models.BaseUser
import com.flife.uresta.models.RawUser
import com.flife.uresta.models.User
import com.flife.uresta.utils.JAKARTA_ZONE_ID
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
interface UserRepository : CommonRepository<RawUser, User> {

    override suspend fun create(user: RawUser): Int

    override suspend fun update(user: RawUser): Boolean

    override suspend fun deleteBy(id: Int): Boolean

    override fun getAll(): Flow<User>

    override fun getBy(id: Int): Flow<User?>
}

class UserRepositoryImpl @Inject constructor(
    private val dao: UserDao
) : UserRepository {

    override suspend fun create(user: RawUser): Int {
        val base = user.toBaseRawUser()

        return dao.create(base)
    }

    override suspend fun update(user: RawUser): Boolean {
        val base = user.toBaseRawUser()

        return dao.update(base)
    }

    override suspend fun deleteBy(id: Int): Boolean {
        return dao.deleteBy(id)
    }

    override fun getAll(): Flow<User> {
        return dao.getAll().map { base -> base.toUser() }
    }

    override fun getBy(id: Int): Flow<User?> {
        return dao.getBy(id).map { base -> base?.toUser() }
    }
}

private fun RawUser.toBaseRawUser() = BaseRawUser(
    id = id,
    email = email,
    loginKey = loginKey,
    name = name,
    birthdate = birthdate,
    gender = gender.ordinal + 1,
    level = level.ordinal + 1,
    lastLoginTime = lastLoginTime?.toLocalDateTime(),
    lastLogoutTime = lastLogoutTime?.toLocalDateTime(),
    isLogin = isLogin,
    profilePictureName = profilePictureName,
    description = description,
    cityId = cityId
)

private fun BaseUser.toUser() = User(
    id = id,
    email = email,
    name = name,
    birthdate = birthdate,
    gender = Gender.values()[gender - 1],
    level = User.Level.values()[level - 1],
    lastLoginTime = lastLoginTime?.atZone(JAKARTA_ZONE_ID),
    lastLogoutTime = lastLogoutTime?.atZone(JAKARTA_ZONE_ID),
    isLogin = isLogin,
    profilePictureName = profilePictureName,
    description = description,
    address = address?.toUserAddress()
)

private fun BaseUser.Address.toUserAddress() =
    User.Address(city.toBriefCity(), country.toBriefCountry())