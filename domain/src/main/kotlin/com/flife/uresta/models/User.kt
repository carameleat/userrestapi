/*
 * Copyright 2021 F: Life
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                      Bismillahirrahmanirrahim

package com.flife.uresta.models

import com.flife.uresta.enums.Gender
import com.flife.uresta.serializers.LocalDateSerializer
import com.flife.uresta.serializers.ZonedDateTimeSerializer
import kotlinx.serialization.Serializable
import java.time.LocalDate
import java.time.ZonedDateTime

@Serializable
data class User(
    val id: Int,
    val email: String,
    val name: String,
    @Serializable(LocalDateSerializer::class)
    val birthdate: LocalDate,
    val gender: Gender, //1 = male, 2 = female
    val level: Level, //1 = admin, 2 = normal user
    @Serializable(ZonedDateTimeSerializer::class)
    val lastLoginTime: ZonedDateTime? = null,
    @Serializable(ZonedDateTimeSerializer::class)
    val lastLogoutTime: ZonedDateTime? = null,
    val isLogin: Boolean = false,
    val profilePictureName: String? = null,
    val description: String? = null,
    val address: Address? = null
) {

    enum class Level {
        ADMIN, NORMAL_USER
    }

    @Serializable
    data class Address(
        val city: BriefCity,
        val country: BriefCountry
    )

    data class LoginResult(
        val userId: Int,
        val refreshTokenId: Int,
        val refreshToken: String
    )
}

@Serializable
data class BaseBriefUser(
    val id: Int,
    val email: String,
    val name: String,
    val level: User.Level
)

@Serializable
data class RawUser(
    val id: Int = 0,
    val email: String,
    val loginKey: String,
    val name: String,
    @Serializable(LocalDateSerializer::class)
    val birthdate: LocalDate,
    val gender: Gender,
    val level: User.Level,
    @Serializable(ZonedDateTimeSerializer::class)
    val lastLoginTime: ZonedDateTime? = null,
    @Serializable(ZonedDateTimeSerializer::class)
    val lastLogoutTime: ZonedDateTime? = null,
    val isLogin: Boolean = false,
    val profilePictureName: String? = null,
    val description: String? = null,
    val cityId: Int = 0
)