/*
 * Copyright 2021 F: Life
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                              Bismillahirrahmanirrahim

package com.flife.uresta.daos

import com.flife.sweet.extensions.getInt
import com.flife.sweet.extensions.getString
import com.flife.uresta.ext.insertedId
import com.flife.uresta.models.BaseBriefCity
import com.flife.uresta.models.BaseBriefCountry
import com.flife.uresta.models.BaseCity
import com.flife.uresta.models.BaseRawCity
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.sql.PreparedStatement
import java.sql.ResultSet
import javax.inject.Inject

@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
interface CityDao : CommonDao<BaseRawCity, BaseCity> {

    override suspend fun create(city: BaseRawCity): Int

    override suspend fun update(city: BaseRawCity): Boolean

    override suspend fun deleteBy(id: Int): Boolean

    override fun getAll(): Flow<BaseCity>

    override fun getBy(id: Int): Flow<BaseCity?>

    fun getCountryOf(cityId: Int): Flow<BaseBriefCountry?>
}

class CityDaoImpl @Inject constructor() : AbstractDao(), CityDao {

    override suspend fun create(city: BaseRawCity): Int {
        val statement = autoWriteInsert(city)

        return conn.use { conn ->
            conn.prepareStatement(statement, PreparedStatement.RETURN_GENERATED_KEYS).use {
               it.executeUpdate()
               it.insertedId
            }
        }
    }

    override suspend fun update(city: BaseRawCity): Boolean {
        val statement = autoWriteUpdate(city) {
            where { BaseRawCity::id isEquals city.id }
        }

        return statement.executeUpdate() > 0
    }

    override suspend fun deleteBy(id: Int): Boolean {
        val statement = writeDelete<BaseRawCity> {
            where { BaseRawCity::id isEquals id }
        }

        return statement.executeUpdate() > 0
    }

    override fun getAll(): Flow<BaseCity> = flow {
        val statement = autoWriteSelect<BaseCity>()
        statement.suspendExecuteQuery { _, _ ->
            while (next()) {
                val city = mapToBaseCity()
                emit(city)
            }
        }
    }

    override fun getBy(id: Int): Flow<BaseCity?> = flow {
        val statement = autoWriteSelect<BaseCity> {
            where { BaseCity::id isEquals id }
        }
        val city = statement.executeQuery { _, _ ->
            if (next()) mapToBaseCity() else null
        }

        emit(city)
    }

    override fun getCountryOf(cityId: Int): Flow<BaseBriefCountry?> = flow {
        val statement = writeSelect {
            take<BaseBriefCountry>()

            from<BaseCity> {
                innerJoin<BaseBriefCountry> {
                    BaseRawCity::countryId isEquals BaseBriefCountry::id
                }
            }

            where { BaseCity::id isEquals cityId }
        }
        val city = statement.executeQuery { _, _ ->
            if (next()) mapToBaseBriefCountry() else null
        }

        emit(city)
    }
}

private fun ResultSet.mapToBaseCity() = BaseCity(
    id = getInt(BaseCity::id),
    name = getString(BaseCity::name),
    description = getString(BaseCity::description),
    country = mapToBaseBriefCountry()
)

internal fun ResultSet.mapToBaseBriefCity() = BaseBriefCity(
    id = getInt(BaseBriefCity::id),
    name = getString(BaseBriefCity::id)
)