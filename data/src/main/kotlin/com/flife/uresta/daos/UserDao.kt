/*
 * Copyright 2021 F: Life
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                      Bismillahirrahmanirrahim

package com.flife.uresta.daos

import com.flife.sweet.extensions.getInt
import com.flife.sweet.extensions.getString
import com.flife.uresta.ext.getLocalDate
import com.flife.uresta.ext.insertedId
import com.flife.uresta.models.*
import com.flife.uresta.utils.Table
import com.flife.uresta.utils.currentJakartaZonedDateTime
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import java.security.MessageDigest
import java.sql.PreparedStatement
import java.sql.ResultSet
import java.sql.SQLException
import javax.inject.Inject
import kotlin.random.Random

@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
interface UserDao : CommonDao<BaseRawUser, BaseUser> {

    override suspend fun create(user: BaseRawUser): Int

    override suspend fun update(user: BaseRawUser): Boolean

    suspend fun updateLoginKey(userId: Int, loginKey: String): Boolean

    override suspend fun deleteBy(id: Int): Boolean

    override fun getAll(): Flow<BaseUser>

    override fun getBy(id: Int): Flow<BaseUser?>

    fun getCityOf(userId: Int): Flow<BaseBriefCity?>

    /**
     * The first value of [Pair] is user's ID and the second is the user's login key.
     */
    suspend fun getLoginKey(email: String): Pair<Int, String>?

    suspend fun login(userId: Int, email: String): BaseUser.LoginResult
}

class UserDaoImpl @Inject constructor() : AbstractDao(), UserDao {

    override suspend fun create(user: BaseRawUser): Int {
        val statement = writeInsert<BaseRawUser> {
            with(user) {
                set(::email)
                set(::loginKey)
                set(::name)
                set(::birthdate)
                set(::gender)
                set(::level)
                set(::description)
                BaseRawUser::cityId becomes if (cityId > 0) cityId else null
            }
        }

        return conn.use { conn ->
            conn.prepareStatement(statement, PreparedStatement.RETURN_GENERATED_KEYS).use {
                it.executeUpdate()
                it.insertedId
            }
        }
    }

    override suspend fun update(user: BaseRawUser): Boolean {
        val statement = writeUpdate<BaseRawUser> {
            with(user) {
                set(::email)
                set(::name)
                set(::birthdate)
                set(::gender)
                set(::level)
                set(::description)
                BaseRawUser::cityId becomes if (cityId > 0) cityId else null

                where { BaseRawUser::id isEquals id }
            }
        }

        return statement.executeUpdate() > 0
    }

    override suspend fun updateLoginKey(userId: Int, loginKey: String): Boolean {
        val statement = writeUpdate<BaseRawUser> {
            BaseRawUser::loginKey becomes loginKey

            where { BaseRawUser::id isEquals userId }
        }

        return statement.executeUpdate() > 0
    }

    override suspend fun deleteBy(id: Int): Boolean {
        val statement = writeDelete<BaseRawUser> {
            where { BaseRawUser::id isEquals id }
        }

        return statement.executeUpdate() > 0
    }

    override fun getAll(): Flow<BaseUser> = flow {
        val statement = writeSelect {
            take3<BaseUser, BaseBriefCity, BaseBriefCountry>()

            from<BaseUser> {
                leftJoin<BaseBriefCity> {
                    BaseRawUser::cityId isEquals BaseBriefCity::id
                }
                leftJoin<BaseBriefCountry> {
                    BaseRawCity::countryId isEquals BaseBriefCountry::id
                }
            }
        }

        statement.suspendExecuteQuery { _, _ ->
            while (next()) {
                val user = mapToBaseUser()
                emit(user)
            }
        }
    }

    override fun getBy(id: Int): Flow<BaseUser?> = flow {
        val statement = writeSelect {
            take3<BaseUser, BaseBriefCity, BaseBriefCountry>()

            from<BaseUser> {
                leftJoin<BaseBriefCity> {
                    BaseRawUser::cityId isEquals BaseBriefCity::id
                }
                leftJoin<BaseBriefCountry> {
                    BaseRawCity::countryId isEquals BaseBriefCountry::id
                }
            }

            where { BaseUser::id isEquals id }
        }

        val user = statement.suspendExecuteQuery { _, _ ->
            if (next()) mapToBaseUser() else null
        }

        emit(user)
    }

    override fun getCityOf(userId: Int): Flow<BaseBriefCity?> = flow {
        val statement = writeSelect {
            take<BaseBriefCity>()

            from<BaseUser> {
                innerJoin<BaseBriefCity> {
                    BaseRawUser::cityId isEquals BaseBriefCity::id
                }
            }

            where { BaseUser::id isEquals userId }
        }
        val user = statement.executeQuery { _, _ ->
            if (next()) mapToBaseBriefCity() else null
        }

        emit(user)
    }

    override suspend fun getLoginKey(email: String): Pair<Int, String>? {
        val statement = writeSelect {
            take(BaseRawUser::id, BaseRawUser::loginKey)

            from<BaseRawUser>()

            where { BaseRawUser::email isEquals email }
        }

        return statement.executeQuery { _, _ ->
            if (next()) {
                val userId = getInt(BaseRawUser::id)
                val loginKey = getString(BaseRawUser::loginKey)

                Pair(userId, loginKey)
            } else null
        }
    }

    override suspend fun login(userId: Int, email: String): BaseUser.LoginResult {
        val updateUserStmt = writeUpdate<BaseRawUser> {
            val jakartaTime = currentJakartaZonedDateTime()

            BaseRawUser::lastLoginTime becomes jakartaTime.toLocalDateTime()
            BaseRawUser::isLogin becomes true

            where { BaseRawUser::id isEquals userId }
        }

        val refreshToken = createRefreshToken(email)
        val insertRefreshTokenStmt = writeInsert(Table.USER_REFRESH_TOKENS) {
            "token" becomes refreshToken
            "user_id" becomes userId
        }

        return conn.use { conn ->
            val autoCommit = conn.autoCommit

            try {
                conn.transaction {
                    executeUpdate(updateUserStmt)

                    val refreshTokenId = prepareStatement(
                        insertRefreshTokenStmt,
                        PreparedStatement.RETURN_GENERATED_KEYS
                    ).use {
                        it.executeUpdate()
                        it.insertedId
                    }

                    BaseUser.LoginResult(userId, refreshTokenId, refreshToken)
                }
            } catch (ex: SQLException) {
                conn.rollback()

                throw ex
            } finally { conn.autoCommit = autoCommit }
        }
    }

    private fun createRefreshToken(email: String): String {
        val builder = StringBuilder()

        for (i in email.indices + Random.nextInt(45, 75)) {
            val index = Random.nextInt(email.lastIndex)
            builder.append(email[index].toString())
        }

        val hash = MessageDigest.getInstance("SHA-512").apply {
            val bytes = builder.toString().toByteArray()
            update(bytes)
        }.digest()

        return builder.apply {
            clear()
            hash.forEach {
                append(it.toHexString())
            }
        }.toString()
    }

    private fun Byte.toHexString(): String = String.format("%02x", this)
}

fun ResultSet.mapToBaseUser() = BaseUser(
    id = getInt(BaseUser::id),
    email = getString(BaseUser::email),
    name = getString(BaseUser::name),
    birthdate = getLocalDate(BaseUser::birthdate),
    gender = getInt(BaseUser::gender),
    level = getInt(BaseUser::level),
    description = getString(BaseUser::description),
    address = BaseUser.Address(mapToBaseBriefCity(), mapToBaseBriefCountry())
)