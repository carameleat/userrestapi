/*
 * Copyright 2021 F: Life
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim


package com.flife.uresta.daos

import com.flife.sweet.extensions.getInt
import com.flife.sweet.extensions.getString
import com.flife.sweet.libs.Mapper
import com.flife.uresta.ext.insertedId
import com.flife.uresta.models.BaseBriefCountry
import com.flife.uresta.models.BaseCountry
import kotlinx.coroutines.flow.*
import java.sql.PreparedStatement
import java.sql.ResultSet
import javax.inject.Inject

@Suppress("PARAMETER_NAME_CHANGED_ON_OVERRIDE")
interface CountryDao : CommonDao<BaseCountry, BaseCountry> {

    override suspend fun create(country: BaseCountry): Int

    override suspend fun update(country: BaseCountry): Boolean

    override suspend fun deleteBy(id: Int): Boolean

    override fun getAll(): Flow<BaseCountry>

    override fun getBy(id: Int): Flow<BaseCountry?>
}

class CountryDaoImpl @Inject constructor() : AbstractDao(), CountryDao {

    override suspend fun create(country: BaseCountry): Int {
        val statement = autoWriteInsert(country)

        return conn.use { conn ->
            conn.prepareStatement(statement, PreparedStatement.RETURN_GENERATED_KEYS).use {
                it.executeUpdate()
                it.insertedId
            }
        }
    }

    override suspend fun update(country: BaseCountry): Boolean {
        val statement = autoWriteUpdate(country) {
            where { BaseCountry::id isEquals country.id }
        }

        return statement.executeUpdate() > 0
    }

    override suspend fun deleteBy(id: Int): Boolean {
        val statement = writeDelete<BaseCountry> {
            where { BaseCountry::id isEquals id }
        }

        return statement.executeUpdate() > 0
    }

    override fun getAll(): Flow<BaseCountry> = flow {
        val statement = autoWriteSelect<BaseCountry>()
        statement.suspendExecuteQuery { _, _ ->
            while (next()) {
                val country = mapToBaseCountry()
                emit(country)
            }
        }
    }

    override fun getBy(id: Int): Flow<BaseCountry?> = flow {
        val statement = autoWriteSelect<BaseCountry> {
            where { BaseCountry::id isEquals id }
        }
        val country = statement.executeQuery { _, _ ->
            if (next()) mapToBaseCountry() else null
        }

        emit(country)
    }
}

private fun ResultSet.mapToBaseCountry() = BaseCountry(
    id = getInt(BaseCountry::id),
    name = getString(BaseCountry::name),
    description = getString(BaseCountry::description)
)

internal fun ResultSet.mapToBaseBriefCountry() = BaseBriefCountry(
    id = getInt(BaseBriefCountry::id),
    name = getString(BaseBriefCountry::name)
)