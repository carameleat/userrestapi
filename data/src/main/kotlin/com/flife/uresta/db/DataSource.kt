/*
 * Copyright 2021 F: Life
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim

package com.flife.uresta.db

import com.flife.uresta.gitignored.DBAuth
import com.zaxxer.hikari.HikariDataSource

object DataSource : HikariDataSource() {

    init {
        driverClassName = "org.h2.Driver"
        jdbcUrl = "jdbc:h2:tcp://localhost/./test/uresta;mode=MySQL;"
        username = DBAuth.USERNAME
        password = DBAuth.PASSWORD
        //In production you should calculate how much connection pool do you need.
        maximumPoolSize = 1
        maxLifetime = 300000 //In production maybe you need a longer maxLifeTime.

        //Uncomment in production + maybe you need a difference configuration.
//        addDataSourceProperty("cachePrepStmts", true)
//        addDataSourceProperty("prepStmtCacheSize", "250")
//        addDataSourceProperty("prepStmtCacheSqlLimit", "2048")
    }
}