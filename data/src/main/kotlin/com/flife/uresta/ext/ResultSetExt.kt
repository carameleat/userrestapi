//                                  Bismillahirrahmanirrahim


package com.flife.uresta.ext

import com.flife.sweet.extensions.tableColumnName
import java.sql.ResultSet
import java.time.LocalDate
import java.time.LocalDateTime
import kotlin.reflect.KProperty1

fun ResultSet.getLocalDate(columnLabel: String): LocalDate =
    getObject(columnLabel, LocalDate::class.java)

inline fun <reified T : Any> ResultSet.getLocalDate(column: KProperty1<T, *>): LocalDate =
    getLocalDate(column.tableColumnName)

fun ResultSet.getLocalDateTime(columnLabel: String): LocalDateTime =
    getObject(columnLabel, LocalDateTime::class.java)

inline fun <reified T : Any> ResultSet.getLocalDateTime(column: KProperty1<T, *>) =
    getLocalDateTime(column.tableColumnName)