//                                  Bismillahirrahmanirrahim


package com.flife.uresta.ext

import java.sql.PreparedStatement

val PreparedStatement.insertedId: Int get() = generatedKeys.use {
    if (it.next()) it.getInt(1) else 0
}