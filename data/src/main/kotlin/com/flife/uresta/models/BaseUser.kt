/*
 * Copyright 2021 F: Life
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                              Bismillahirrahmanirrahim

package com.flife.uresta.models

import com.flife.sweet.annotations.From
import com.flife.sweet.annotations.Ignore
import com.flife.sweet.annotations.Name
import com.flife.sweet.enums.Operation
import com.flife.uresta.utils.Table
import java.time.LocalDate
import java.time.LocalDateTime

@From(Table.USERS)
data class BaseUser(
    val id: Int,
    val email: String,
    val name: String,
    val birthdate: LocalDate,
    val gender: Int, //1 = male, 2 = female
    val level: Int, //1 = admin, 2 = normal user
    @Name("last_login_time")
    val lastLoginTime: LocalDateTime? = null,
    @Name("last_logout_time")
    val lastLogoutTime: LocalDateTime? = null,
    @Name("is_login")
    val isLogin: Boolean = false,
    @Name("profile_picture_name")
    val profilePictureName: String? = null,
    val description: String? = null,
    @Ignore
    val address: Address? = null
) {

    data class Address(
        val city: BaseBriefCity,
        val country: BaseBriefCountry
    )

    data class LoginResult(
        val userId: Int,
        val refreshTokenId: Int,
        val refreshToken: String
    )
}

@From(Table.USERS)
data class BaseBriefUser(
    val id: Int,
    val email: String,
    val name: String,
    val level: Int
)

@From(Table.USERS)
data class BaseRawUser(
    @Ignore(Operation.INSERT, Operation.UPDATE)
    val id: Int = 0,
    val email: String,
    @Name("login_key")
    val loginKey: String,
    val name: String,
    val birthdate: LocalDate,
    val gender: Int,
    val level: Int,
    @Name("last_login_time")
    val lastLoginTime: LocalDateTime? = null,
    @Name("last_logout_time")
    val lastLogoutTime: LocalDateTime? = null,
    @Name("is_login")
    val isLogin: Boolean = false,
    @Name("profile_picture_name")
    val profilePictureName: String? = null,
    val description: String? = null,

    /**
     * If the value is 0 then record it as null in the database.
     */
    @Name("city_id")
    val cityId: Int = 0
)