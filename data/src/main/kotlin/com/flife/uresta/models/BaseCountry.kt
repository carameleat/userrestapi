/*
 * Copyright 2021 F: Life
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */

//                                  Bismillahirrahmanirrahim

package com.flife.uresta.models

import com.flife.sweet.annotations.From
import com.flife.sweet.annotations.Ignore
import com.flife.sweet.enums.Operation
import com.flife.uresta.utils.Table

@From(Table.COUNTRIES)
data class BaseCountry(
    @Ignore(Operation.INSERT, Operation.UPDATE)
    val id: Int = 0,
    val name: String,
    val description: String? = null
)

@From(Table.COUNTRIES)
data class BaseBriefCountry(
    val id: Int,
    val name: String
)