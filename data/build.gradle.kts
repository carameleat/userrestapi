//                                  Bismillahirrahmanirrahim


import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    kotlin("kapt")
}

val rest_api_version: String by project
group = "com.flife"
version = rest_api_version

tasks.withType<KotlinCompile>().all {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

//repositories {
//    flatDir {
//        dirs("../libs")
//    }
//}

val log4j_api_version: String by project
val dagger_version: String by project
val coroutines_version: String by project
val serialization_version: String by project
val h2_version: String by project
dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    //Kotlinx Coroutines
    implementation("org.jetbrains.kotlinx:kotlinx-coroutines-core-jvm:$coroutines_version")

    //Sweet
    api(files("libs/sweet-h2-1.0-beta03.jar"))

    //H2 Database
    implementation("com.h2database:h2:$h2_version")

    //HikariCP
    implementation("com.zaxxer:HikariCP:3.4.5")

    //Dagger
    api("com.google.dagger:dagger:$dagger_version")
    kapt("com.google.dagger:dagger-compiler:$dagger_version")

    //Log4J API
    api("org.apache.logging.log4j:log4j-core:$log4j_api_version")
    api("org.apache.logging.log4j:log4j-api:$log4j_api_version")
    implementation("org.apache.logging.log4j:log4j-slf4j-impl:$log4j_api_version")

    //SLF4J for removing conflic dependency
    api("org.slf4j:slf4j-api:1.7.30")

    testImplementation("junit", "junit", "4.13")
}