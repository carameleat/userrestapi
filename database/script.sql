-- Bismillahirrahmanirrahim


create table countries(
	id int not null primary key auto_increment,
	name varchar(150) not null,
	description varchar(2500) null
);

create table cities(
	id int not null primary key auto_increment,
	name varchar(150) not null,
	description varchar(2500) null,
	country_id int not null,
	constraint fk_cities_coutry_id foreign key(country_id)
		references countries(id) on update cascade on delete cascade
);

create table users(
	id int not null primary key auto_increment,
	email varchar(150) not null,
	login_key varchar(60) not null,
	name varchar(150) not null,
	birthdate date not null,
	gender tinyint not null, -- 1 = male, 2 = female
	level int not null, -- 1 = admin, 2 = normal user
	last_login_time datetime null,
	last_logout_time datetime null,
	is_login boolean not null,
	profile_picture_name varchar(4096) null,
	description varchar(250) null,
	city_id int null, -- null means the city which is not recorded.
	constraint fk_users_city_id foreign key(city_id)
		references cities(id) on update cascade on delete set null,
	unique key unq_users_email (email)
);

create table user_refresh_tokens(
	id int not null primary key auto_increment,
	token varchar(128) not null,
	user_id int not null,
	constraint fk_user_refresh_tokens_user_id foreign key(user_id)
		references users(id) on update cascade on delete cascade
);