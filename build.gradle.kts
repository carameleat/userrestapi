//                                          Bismillahirrahmanirrahim


buildscript {
    repositories {
        jcenter()
    }
    dependencies {
        classpath(embeddedKotlin("gradle-plugin"))
        classpath(embeddedKotlin("serialization"))
    }
}

plugins {
    kotlin("jvm") version "1.4.30"
    kotlin("kapt") version "1.4.30"
}

val rest_api_version: String by project
group = "com.flife"
version = rest_api_version

allprojects {
    repositories {
        jcenter()
    }
}