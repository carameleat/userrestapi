//                                      Bismillahirrahmanirrahim


import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    application
    kotlin("jvm")
    kotlin("kapt")
    kotlin("plugin.serialization")
}

application {
    mainClassName = "io.ktor.server.netty.EngineMain"
}

val rest_api_version: String by project
group = "com.flife"
version = rest_api_version

tasks.withType<KotlinCompile>().all {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

val ktor_version: String by project
val dagger_version: String by project
val log4j_api_version: String by project
dependencies {
    val netty_version = "2.0.36.Final"

    implementation(kotlin("stdlib-jdk8"))

    //Ktor
    implementation("io.ktor:ktor-server-netty:$ktor_version")
    implementation("io.ktor:ktor-server-core:$ktor_version")
    implementation("io.ktor:ktor-server-host-common:$ktor_version")
    implementation("io.ktor:ktor-auth:$ktor_version")
    implementation("io.ktor:ktor-auth-jwt:$ktor_version")
    implementation("io.ktor:ktor-serialization:$ktor_version")
    testImplementation("io.ktor:ktor-server-tests:$ktor_version")

    //Netty for SSL
    implementation("io.netty:netty-tcnative:$netty_version")
    implementation("io.netty:netty-tcnative-boringssl-static:$netty_version:windows-x86_64")

    implementation(project(":domain"))

    //Dagger
    kapt("com.google.dagger:dagger-compiler:$dagger_version")

    //Log4j SLF4J implementation
    implementation("org.apache.logging.log4j:log4j-slf4j-impl:$log4j_api_version")
}
